﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.DataAccessLayer.Abstract
{
    public interface IRepository<T>
    {
        List<T> List();
        T FindById(ObjectId id);
        T Insert(T obj);
        void Update(ObjectId id, T obj);
        void Delete(ObjectId id);
        List<T> InsertMany(List<T> obj);
    }
}
