﻿using StudentMatchingPlatform.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using StudentMatchingPlatform.DataAccessLayer.Abstract;
using MongoDB.Bson;
using System.Collections.ObjectModel;

namespace StudentMatchingPlatform.DataAccessLayer.MongoDB
{
    public class StudentRepository: RepositoryBase, IRepository<Students>
    {
        private IMongoCollection<Students> _students;

        public StudentRepository()
        {
            _students = db.Students;
        }

        public List<Students> List()
        {
            return _students.Find(student => true).ToList();
        }

        public Students FindById(ObjectId id)
        {
            return _students.Find<Students>(student => student._id.Equals(id)).FirstOrDefault();
        }

        public Students FindByGuid(Guid guid)
        {
            return _students.Find<Students>(student => student.ActivateGuid == guid).FirstOrDefault();
        }

        public Students FindByEmail(string email)
        {
            return _students.Find<Students>(student => student.Email == email).FirstOrDefault();
        }

        public Students FindByPassword(string password)
        {
            return _students.Find<Students>(student => student.Password == password).FirstOrDefault();
        }

        public Students Login(string email, string password)
        {
            return _students.Find<Students>(x => x.Email == email && x.Password == password).FirstOrDefault();
        }

        public Students Insert(Students student)
        {
            _students.InsertOne(student);
            return student;
        }

        public void Update(ObjectId id, Students studentIn)
        {
            _students.ReplaceOne(student => student._id.Equals(id), studentIn);  
        }

        public void Delete(ObjectId id)
        {
            _students.DeleteOne(student => student._id.Equals(id));
        }

        public void DeletePreviousMatch(ObjectId current_id, ObjectId previous_id)
        {
            var update = Builders<Students>.Update.PullFilter(p => p.PreviousMatches,
                                                f => f.PreviousMatchingStudentsId == previous_id);

            _students.FindOneAndUpdate(p => p._id == current_id, update);
        }

        public List<Students> InsertMany(List<Students> obj)
        {
            throw new NotImplementedException();
        }
    }
}
