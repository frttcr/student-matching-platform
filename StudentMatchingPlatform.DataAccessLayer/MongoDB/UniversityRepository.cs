﻿using MongoDB.Bson;
using MongoDB.Driver;
using StudentMatchingPlatform.DataAccessLayer.Abstract;
using StudentMatchingPlatform.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.DataAccessLayer.MongoDB
{
    public class UniversityRepository : RepositoryBase, IRepository<Universities>
    {
        private IMongoCollection<Universities> _universities;

        public UniversityRepository()
        {
            _universities = db.Universities;
        }

        public void Delete(ObjectId id)
        {
            _universities.DeleteOne(university => university.Id.Equals(id));
        }

        public Universities FindById(ObjectId id)
        {
            return _universities.Find<Universities>(university => university.Id.Equals(id)).FirstOrDefault();
        }

        public Universities Insert(Universities university)
        {
            _universities.InsertOne(university);
            return university;
        }

        public List<Universities> List()
        {
            return _universities.Find(university => university.Country.Equals("Turkey")).SortBy(university => university.Name).ToList();
        }

        public void Update(ObjectId id, Universities universityIn)
        {
            _universities.ReplaceOne(university => university.Id.Equals(id), universityIn);
        }

        public List<Universities> InsertMany(List<Universities> universities)
        {
            _universities.InsertMany(universities);
            return universities;
        }
    }
}
