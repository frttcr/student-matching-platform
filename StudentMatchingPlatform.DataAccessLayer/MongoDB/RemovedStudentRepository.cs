﻿using MongoDB.Bson;
using MongoDB.Driver;
using StudentMatchingPlatform.DataAccessLayer.Abstract;
using StudentMatchingPlatform.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.DataAccessLayer.MongoDB
{
    public class RemovedStudentRepository : RepositoryBase, IRepository<Students>
    {
        private IMongoCollection<Students> _removedStudents;

        public RemovedStudentRepository()
        {
            _removedStudents = db.RemovedStudents;
        }

        public void Update(ObjectId id, Students studentIn)
        {
            _removedStudents.ReplaceOne(student => student._id.Equals(id), studentIn);
        }

        public void Delete(ObjectId id)
        {
            _removedStudents.DeleteOne(student => student._id.Equals(id));
        }

        public Students FindById(ObjectId id)
        {
            return _removedStudents.Find<Students>(student => student._id.Equals(id)).FirstOrDefault();
        }

        public Students Insert(Students student)
        {
            _removedStudents.InsertOne(student);
            return student;
        }

        public List<Students> InsertMany(List<Students> obj)
        {
            throw new NotImplementedException();
        }

        public List<Students> List()
        {
            return _removedStudents.Find(student => true).ToList();
        }

    }
}
