﻿using MongoDB.Driver;
using StudentMatchingPlatform.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.DataAccessLayer.MongoDB
{
    public class MongoDBContext
    {
        private readonly IMongoDatabase _database = null;
        private static string connectionString = ConfigurationManager.ConnectionStrings["MongoDBContext"].ConnectionString;    

        public MongoDBContext()
        {
            var client = new MongoClient(connectionString);
            if(client != null)
            {
                _database = client.GetDatabase("sms");
            }
        }

        public IMongoCollection<Students> Students
        {
            get
            {
                return _database.GetCollection<Students>("Students");
            }
        }

        public IMongoCollection<Universities> Universities
        {
            get
            {
                return _database.GetCollection<Universities>("Universities");
            }
        }

        public IMongoCollection<Students> RemovedStudents
        {
            get
            {
                return _database.GetCollection<Students>("RemovedStudents");
            }
        }
    }
}
