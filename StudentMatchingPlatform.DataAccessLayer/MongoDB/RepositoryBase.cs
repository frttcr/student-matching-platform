﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.DataAccessLayer.MongoDB
{
    public class RepositoryBase
    {
        protected static MongoDBContext db;
        private static object _lockSync = new object();

        // Protector constructor for singleton pattern: to avoid create new object
        protected RepositoryBase()
        {
            CreateContext();
        }

        private static void CreateContext()
        {
            if(db == null)
            {
                lock(_lockSync)
                {
                    if(db == null)
                    {
                        db = new MongoDBContext();
                    }
                }
            }
        }
    }
}
