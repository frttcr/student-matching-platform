﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities
{
    public class PreviousMatches
    {
        [BsonElement("previousMatchingStudentId")]
        public ObjectId PreviousMatchingStudentsId { get; set; }
        [BsonElement("createdOn")]
        public DateTime CreatedOn { get; set; }

        public PreviousMatches(ObjectId previousMatchingStudentsId, DateTime createdOn)
        {
            this.PreviousMatchingStudentsId = previousMatchingStudentsId;
            this.CreatedOn = createdOn;
        }
    }
}
