﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities
{
    public class InterestArea
    {
        [BsonElement("name")]
        public string Name { get; set; }
        [BsonElement("createdOn")]
        public DateTime CreatedOn { get; set; }

        public InterestArea(string name, DateTime createdOn)
        {
            this.Name = name;
            this.CreatedOn = createdOn;
        }
    }
}
