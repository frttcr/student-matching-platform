﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities.Messages
{
    public class ErrorMessageObject
    {
        public ErrorMessageCodes Code { get; set; }
        public string Message;
    }
}
