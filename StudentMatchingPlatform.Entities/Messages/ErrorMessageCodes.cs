﻿namespace StudentMatchingPlatform.Entities.Messages
{
    public enum ErrorMessageCodes
    {
        EmailAlreadyExist = 101,
        UserIsNotActive = 102,
        EmailOrPasswordWrong = 103,
        CheckEmailAddress = 104,
        UserAlreadyActive = 105,
        ActivateIdDoesNotExist = 106,
        UserNotFound = 107,
        UserCouldNotFound = 108,
        OutOfMatchNumber = 109,
        NullInfo = 110,
        EmailNotExist = 111,
        NotUniMailAddress = 112
    }
}
