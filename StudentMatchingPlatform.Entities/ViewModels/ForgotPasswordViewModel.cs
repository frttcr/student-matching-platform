﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [DisplayName("Email Address"),
            Required(ErrorMessage = "{0} cannot empty."),
            DataType(DataType.EmailAddress),
            EmailAddress(ErrorMessage = "Please enter valid e-mail address.")]
        public string Email { get; set; }
    }
}
