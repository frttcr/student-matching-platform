﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentMatchingPlatform.Entities.ViewModels
{
    public class RegisterViewModel
    {
        [DisplayName("Email Address"),
            Required(ErrorMessage = "{0} cannot empty."),
            StringLength(256, ErrorMessage = "Email max 256 char"),
            DataType(DataType.EmailAddress),
            EmailAddress(ErrorMessage = "Please enter valid e-mail address.")]
        public string Email { get; set; }
        [DisplayName("Password"), Required(ErrorMessage = "{0} cannot be empty."), StringLength(64, ErrorMessage = "Password max 64 char"), DataType(DataType.Password)]
        public string Password { get; set; }
        [DisplayName("Re-Password"), Required(ErrorMessage = "{0} cannot be empy."), StringLength(64, ErrorMessage = "Password max 64 char"), DataType(DataType.Password), Compare("Password", ErrorMessage = "Password did not match.")]
        public string RePassword { get; set; }
    }
}