﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities.ViewModels
{
    public class LoginViewModel
    {
        [DisplayName("Email address"),
            Required(ErrorMessage = "{0} cannot empty."),
            DataType(DataType.EmailAddress),
            EmailAddress(ErrorMessage = "Please enter valid e-mail address.")]
        public string Email { get; set; }
        [DisplayName("Password"), Required(ErrorMessage = "{0} cannot be empty."), StringLength(64, ErrorMessage = "Password max 64 char"), DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
