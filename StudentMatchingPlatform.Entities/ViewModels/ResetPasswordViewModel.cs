﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities.ViewModels
{
    public class ResetPasswordViewModel
    {
        [DisplayName("Password"), Required(ErrorMessage = "{0} cannot be empty."), StringLength(64, ErrorMessage = "Password max 64 char"), DataType(DataType.Password)]
        public string Password { get; set; }
        [DisplayName("Re-Password"), Required(ErrorMessage = "{0} cannot be empy."), StringLength(64, ErrorMessage = "Password max 64 char"), DataType(DataType.Password), Compare("Password", ErrorMessage = "Password did not match.")]
        public string RePassword { get; set; }
    }
}
