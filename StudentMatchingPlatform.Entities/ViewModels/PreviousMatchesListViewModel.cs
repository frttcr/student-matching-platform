﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities.ViewModels
{
    public class PreviousMatchesListViewModel
    {
        public List<Students> Students { get; set; }

        public PreviousMatchesListViewModel()
        {
            Students = new List<Students>();
        }
    }
}
