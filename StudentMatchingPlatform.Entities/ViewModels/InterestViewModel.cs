﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities.ViewModels
{
    public class InterestViewModel
    {
        [DisplayName("Interest Area")]
        public string Name { get; set; }
    }
}
