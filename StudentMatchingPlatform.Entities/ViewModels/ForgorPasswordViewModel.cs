﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities.ViewModels
{
    public class ForgorPasswordViewModel
    {
        [DisplayName("E-Mail Address")]
        public string Email { get; set; }
    }
}
