﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities.ViewModels
{
    public class EditProfileViewModel
    {
        public Students Student { get; set; }
        public List<Universities> Universities { get; set; }
    }
}
