﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities
{
    public class Students
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("_id")]    
        public ObjectId _id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("surname")]
        public string Surname { get; set; }

        [BsonElement("dob")]
        public string DOB { get; set; }

        [BsonElement("university")]
        public string University { get; set; }

        [BsonElement("department")]
        public string Department { get; set; }

        [BsonElement("class")]
        public string Class { get; set; }

        [BsonElement("gpa")]
        public string GPA { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("isAdmin")]
        public bool IsAdmin { get; set; }

        [BsonElement("isActive")]
        public bool IsActive { get; set; }

        [BsonElement("activateGuid")]
        public Guid ActivateGuid { get; set; }

        [BsonElement("createdOn")]
        public DateTime CreatedOn { get; set; }

        [BsonElement("modifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [BsonElement("modifiedUsername")]
        public string ModifiedUsername { get; set; }

        [BsonElement("interestArea")]
        public List<InterestArea> InterestArea { get; set; }

        [BsonElement("countOfMatch")]
        public int CountOfMatch { get; set; }

        [BsonElement("matches")]
        public List<Matches> Matches { get; set; }

        [BsonElement("previousMatches")]
        public List<PreviousMatches> PreviousMatches { get; set; }
    }
}
