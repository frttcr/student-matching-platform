﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.Entities
{
    public class Matches
    {
        [BsonElement("matchingStudentId")]
        public ObjectId MatchingStudentsId { get; set; }
        [BsonElement("createdOn")]
        public DateTime CreatedOn { get; set; }

        public Matches(ObjectId matchingStudentsId, DateTime createdOn)
        {
            this.MatchingStudentsId = matchingStudentsId;
            this.CreatedOn = createdOn;
        }
    }
}
