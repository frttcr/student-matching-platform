﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace StudentMatchingPlatform.Common.Helpers
{
    //To access web.app 's web config values like mailport, host..
    // Generic for access string or int..
    public class ConfigurationHelper
    {
        public static T Get<T>(string key)
        {
            return (T)Convert.ChangeType(ConfigurationManager.AppSettings[key], typeof(T));
        }
    }
}
