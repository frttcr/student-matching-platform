﻿using MongoDB.Bson;
using StudentMatchingPlatform.Common;
using StudentMatchingPlatform.Common.Helpers;
using StudentMatchingPlatform.DataAccessLayer.MongoDB;
using StudentMatchingPlatform.Entities;
using StudentMatchingPlatform.Entities.Messages;
using StudentMatchingPlatform.Entities.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.BusinessLayer
{
    public class StudentManager
    {
        private StudentRepository studentRepo = new StudentRepository();
        private RemovedStudentRepository removedStudentRepo = new RemovedStudentRepository();

        public BusinessLayerResult<Students> RegisterStudent(RegisterViewModel data)
        {
            Students student = studentRepo.FindByEmail(data.Email);
            BusinessLayerResult<Students> layerResult = new BusinessLayerResult<Students>();

            if (student != null)
            {
                if (student.Email == data.Email)
                {
                    layerResult.AddError(ErrorMessageCodes.EmailAlreadyExist, "Email address was received.");
                }
            }
            else
            {
                //To check university mail address
                if (!data.Email.Contains("edu"))
                {
                    layerResult.AddError(ErrorMessageCodes.NotUniMailAddress, "Please enter university mail address.");
                    return layerResult;
                }
                else
                {
                    studentRepo.Insert(new Students()
                    {
                        Email = data.Email,
                        Password = data.Password,
                        ActivateGuid = Guid.NewGuid(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        IsActive = false,
                        IsAdmin = false,
                        ModifiedUsername = App.Common.GetEmail(),
                        CountOfMatch = 0
                    });
                }
               

                // Send Activation mail..
                layerResult.Result = studentRepo.FindByEmail(data.Email);

                string siteUri = ConfigurationHelper.Get<string>("SiteRootUri");
                string activateUri = $"{siteUri}/Home/UserActivate/{layerResult.Result.ActivateGuid}";
                string body = $"<a href='{activateUri}' target='_blank'> Click to activate your account.</a>";
                MailHelper.SendMail(body, layerResult.Result.Email, "Activate Your Student Matching Platform Account", true);
            }

            return layerResult;
        }

        public BusinessLayerResult<Students> ActivateStudent(Guid activateId)
        {
            BusinessLayerResult<Students> layerResult = new BusinessLayerResult<Students>();

            layerResult.Result = studentRepo.FindByGuid(activateId);

            if (layerResult.Result != null)
            {
                if (layerResult.Result.IsActive)
                {
                    layerResult.AddError(ErrorMessageCodes.UserAlreadyActive, "Your account already active.");
                    return layerResult;
                }
                layerResult.Result.IsActive = true;
                studentRepo.Update(layerResult.Result._id, layerResult.Result);
            }
            else
            {
                layerResult.AddError(ErrorMessageCodes.ActivateIdDoesNotExist, "Activate ID does not exist.");
            }
            return layerResult;
        }

        public BusinessLayerResult<Students> ForgotPassword(ForgotPasswordViewModel model)
        {
            Students student = studentRepo.FindByEmail(model.Email);
            BusinessLayerResult<Students> layerResult = new BusinessLayerResult<Students>();
           // layerResult.Result = studentRepo.FindByEmail

            if (student == null)
            {
                layerResult.AddError(ErrorMessageCodes.EmailNotExist,"There is no student registered with that email addresss.");
                return layerResult;
            }

            // Send Reset Password mail..
            layerResult.Result = studentRepo.FindByEmail(model.Email);

            string siteUri = ConfigurationHelper.Get<string>("SiteRootUri");
            string activateUri = $"{siteUri}/Home/ResetPassword/{layerResult.Result.ActivateGuid}";
            string body = $"<a href='{activateUri}' target='_blank'> Click to reset your password.</a>";
            MailHelper.SendMail(body, layerResult.Result.Email, "Reset Your Student Matching Platform Account", true);

            return layerResult;
        }

        public BusinessLayerResult<Students> ResetPassword(Guid activateId, ResetPasswordViewModel model)
        {
            BusinessLayerResult<Students> layerResult = new BusinessLayerResult<Students>();

            layerResult.Result = studentRepo.FindByGuid(activateId);

            if (layerResult.Result != null)
            {
                layerResult.Result.Password = model.Password;
                studentRepo.Update(layerResult.Result._id, layerResult.Result);
            }
            else
            {
                layerResult.AddError(ErrorMessageCodes.ActivateIdDoesNotExist, "Activate ID does not exist.");
            }
            return layerResult;
        }

        public BusinessLayerResult<Students> LoginStudent(LoginViewModel data)
        {
            Students student = studentRepo.Login(data.Email, data.Password);

            BusinessLayerResult<Students> layerResult = new BusinessLayerResult<Students>();

            layerResult.Result = student;

            if (student != null)
            {
                if (!student.IsActive)
                {
                    layerResult.AddError(ErrorMessageCodes.UserIsNotActive, "Your account is not active.");
                    layerResult.AddError(ErrorMessageCodes.CheckEmailAddress, "Please control your email account.");
                }
            }
            else
            {
                layerResult.AddError(ErrorMessageCodes.EmailOrPasswordWrong, "Email or password did not match.");
            }

            return layerResult;
        }

        public BusinessLayerResult<Students> GetStudentById(ObjectId id)
        {
            BusinessLayerResult<Students> layerResult = new BusinessLayerResult<Students>();

            layerResult.Result = studentRepo.FindById(id);

            if (layerResult.Result == null)
            {
                layerResult.AddError(ErrorMessageCodes.UserNotFound, "Student not found.");
            }

            return layerResult;
        }

        public BusinessLayerResult<Students> UpdateProfile(EditProfileViewModel model)
        {
            Students db_student = studentRepo.FindByEmail(model.Student.Email);
            BusinessLayerResult<Students> result = new BusinessLayerResult<Students>();

            result.Result = studentRepo.FindById(db_student._id);

            result.Result.Name = model.Student.Name;
            result.Result.Surname = model.Student.Surname;
            result.Result.DOB = model.Student.DOB;
            result.Result.University = model.Student.University;
            result.Result.Department = model.Student.Department;
            result.Result.Class = model.Student.Class;
            result.Result.GPA = model.Student.GPA;
            result.Result.ModifiedOn = DateTime.Now;
            result.Result.ModifiedUsername = App.Common.GetEmail();

            studentRepo.Update(result.Result._id, result.Result);

            return result;
        }

        public BusinessLayerResult<Students> UpdateInterestArea(ObjectId id, InterestViewModel model)
        {
            Students db_student = studentRepo.FindById(id);
            BusinessLayerResult<Students> result = new BusinessLayerResult<Students>();

            result.Result = studentRepo.FindById(db_student._id);

            if (result.Result.InterestArea == null)
            {
                result.Result.InterestArea = new List<InterestArea>();
            }

            if (result.Result.InterestArea.Count >= 1)
            {
                result.AddError(ErrorMessageCodes.OutOfMatchNumber, "You can match only one student.");
                return result;
            }
            else if (result.Result.Name == null)
            {
                result.AddError(ErrorMessageCodes.NullInfo, "You should enter your informations.");
                return result;   
            }
            else
            {
                result.Result.InterestArea.Add(new InterestArea(model.Name, DateTime.Now));

                studentRepo.Update(result.Result._id, result.Result);

                return result;
            } 
        }

        public BusinessLayerResult<Students> RemoveUserByID(ObjectId id)
        {
            BusinessLayerResult<Students> result = new BusinessLayerResult<Students>();
            Students student = studentRepo.FindById(id);

            if (student == null)
            {
                result.AddError(ErrorMessageCodes.UserCouldNotFound, "User could not found");
                return result;
            }

            removedStudentRepo.Insert(student);

            // If student has a match, when removed account their friends match should be removed.
            if(student.Matches != null)
            {
                var stuFriend = studentRepo.FindById(student.Matches[0].MatchingStudentsId);

                if (stuFriend.PreviousMatches == null)
                {
                    stuFriend.PreviousMatches = new List<PreviousMatches>();
                }

                stuFriend.PreviousMatches.Add(new PreviousMatches(student._id, DateTime.Now));
                stuFriend.CountOfMatch -= 1;
                stuFriend.InterestArea = null;
                stuFriend.Matches = null;

                studentRepo.Update(stuFriend._id, stuFriend);
            }

            studentRepo.Delete(id);

            return result;
        }

        public BusinessLayerResult<Students> GetStudentMatchingFriend(ObjectId id)
        {
            BusinessLayerResult<Students> layerResult = new BusinessLayerResult<Students>();
            Students currentStudent = studentRepo.FindById(id);

            if(currentStudent.Matches != null)
            {
                layerResult.Result = studentRepo.FindById(currentStudent.Matches[0].MatchingStudentsId);
            }
            else
            {
                layerResult.AddError(ErrorMessageCodes.UserNotFound, "You have not any matching now.");
                return layerResult;
            }

            return layerResult;
        }

        public List<Students> GetPreviousStudentMatchingFriend(ObjectId id)
        {
            List<Students> previousStudents = new List<Students>();
            Students currentStudent = studentRepo.FindById(id);

            if (currentStudent.PreviousMatches != null)
            {
                var x = currentStudent.PreviousMatches.Count;
                for (int i = 0; i<x; i++)
                {
                    // Previous matched friend can delete their account.
                    if (studentRepo.FindById(currentStudent.PreviousMatches[i].PreviousMatchingStudentsId) != null)
                    {
                        Students stu = studentRepo.FindById(currentStudent.PreviousMatches[i].PreviousMatchingStudentsId);

                        previousStudents.Add(stu);
                    }
                    else
                    {
                        // if previous matches delete their account, remove previous student for current students.
                     /*   studentRepo.DeletePreviousMatch(currentStudent._id, currentStudent.PreviousMatches[i].PreviousMatchingStudentsId);*/

                        //TODO: if previousmatches.count == 0 then null to previous match
                        /*
                         if (currentStudent.PreviousMatches.Count == 0)
                          {
                              currentStudent.PreviousMatches = null;
                               studentRepo.Update(currentStudent._id, currentStudent);
                          }
                          */
                        Students stu = removedStudentRepo.FindById(currentStudent.PreviousMatches[i].PreviousMatchingStudentsId);

                        previousStudents.Add(stu);
                    }


                }
            }

            return previousStudents;
        }


        public void Algorithm()
        {
            List<Students> students = studentRepo.List();

            // Dictionary keep only objectId and interest area
            Dictionary<ObjectId, string> studentDictionary = new Dictionary<ObjectId, string>();

            foreach (var student in students)
            {
                if (student.InterestArea != null && student.CountOfMatch == 0 && student.Matches == null)
                {
                    studentDictionary.Add(student._id, student.InterestArea[0].Name);
                }
            }

            foreach (KeyValuePair<ObjectId, string> stu in studentDictionary.ToList())
            {
                ObjectId id = stu.Key;
                string interest = stu.Value;    
                studentDictionary.Remove(stu.Key);

                foreach (KeyValuePair<ObjectId, string> stuFriend in studentDictionary.ToList())
                {
                    ObjectId idFriend = stuFriend.Key;
                    string interestFriend = stuFriend.Value;

                //    if (interest != interestFriend)
                //    {
                        Students student1 = new Students();
                        student1 = studentRepo.FindById(id); ;
                        Students student2 = new Students();
                        student2 = studentRepo.FindById(idFriend); ;

                        if (student1.Matches == null && student2.Matches == null)
                        {
                            student1.Matches = new List<Matches>();
                            student2.Matches = new List<Matches>();
                        }

                        DateTime time = DateTime.Now;

                        if(student1.CountOfMatch == 0 && student1.InterestArea != null && student2.CountOfMatch == 0 && student2.InterestArea != null  && (student1.InterestArea[0].Name == student2.Department) && (student1.Department == student2.InterestArea[0].Name))
                        {
                            student1.Matches.Add(new Matches(student2._id, time));
                            student1.CountOfMatch += 1;
                            studentRepo.Update(student1._id, student1);

                            student2.Matches.Add(new Matches(student1._id, time));
                            student2.CountOfMatch += 1;
                            studentRepo.Update(student2._id, student2);
                            studentDictionary.Remove(stuFriend.Key);
                        }
                        //break;
                    //}
                }
            }
            studentDictionary.Clear();
        }

        public BusinessLayerResult<Students> RemoveUserMatchByID(ObjectId id)
        {
            BusinessLayerResult<Students> result = new BusinessLayerResult<Students>();
            Students student = studentRepo.FindById(id);
            Students StudentsFriend = studentRepo.FindById(student.Matches[0].MatchingStudentsId);

            if (student.Matches.Count == 0 || student.Matches == null)
            {
                result.AddError(ErrorMessageCodes.UserCouldNotFound, "Matching could not found");
                return result;
            }

            if (student.PreviousMatches == null)
            {
                student.PreviousMatches = new List<PreviousMatches>();
            }
            if (StudentsFriend.PreviousMatches == null)
            {
                StudentsFriend.PreviousMatches = new List<PreviousMatches>();
            }

            // When one student remove the match:
            // 1. Add previous match to removed match / also friend's too.
            // 2. Decrase the count of match to 0.
            // 3. Remove the interest area.
            DateTime time = DateTime.Now;

            student.PreviousMatches.Add(new PreviousMatches(StudentsFriend._id, time));
            student.CountOfMatch -= 1;
            student.InterestArea = null;
            student.Matches = null;

            StudentsFriend.PreviousMatches.Add(new PreviousMatches(student._id, time));
            StudentsFriend.CountOfMatch -= 1;
            StudentsFriend.InterestArea = null;
            StudentsFriend.Matches = null;

            studentRepo.Update(student._id, student);
            studentRepo.Update(StudentsFriend._id, StudentsFriend);

            return result;
        }

    }
}
