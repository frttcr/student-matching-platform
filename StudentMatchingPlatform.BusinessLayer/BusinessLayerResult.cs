﻿using StudentMatchingPlatform.Entities.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.BusinessLayer
{
    public class BusinessLayerResult<T> where T : class
    {
        // Purpose of error message obejct: separete for all error messages
        public List<ErrorMessageObject> Errors { get; set; }
        public T Result { get; set; }
       
        public BusinessLayerResult()
        {
            Errors = new List<ErrorMessageObject>();
        }

        public void AddError(ErrorMessageCodes code, string message)
        {
            Errors.Add(new ErrorMessageObject() { Code = code, Message = message });
        }
    }
}
