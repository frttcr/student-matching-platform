﻿using StudentMatchingPlatform.DataAccessLayer.MongoDB;
using StudentMatchingPlatform.Entities;
using StudentMatchingPlatform.Entities.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMatchingPlatform.BusinessLayer
{
    public class UniversityManager
    {
        private UniversityRepository universityRepo = new UniversityRepository();

        public BusinessLayerResult<List<Universities>> GetUniversities()
        {
            BusinessLayerResult<List<Universities>> layerResult = new BusinessLayerResult<List<Universities>>();

            layerResult.Result = universityRepo.List();

            if (layerResult.Result == null)
            {
                layerResult.AddError(ErrorMessageCodes.UserNotFound, "Universites could not found.");
            }

            return layerResult;
        }
    }
}
