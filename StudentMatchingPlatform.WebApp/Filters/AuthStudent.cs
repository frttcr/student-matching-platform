﻿using StudentMatchingPlatform.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentMatchingPlatform.WebApp.Filters
{
    public class AuthStudent : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {         
            if (HttpContext.Current.Session["login"] as Students == null)
            {
                filterContext.Result = new RedirectResult("/Home/Login");
            }
        }
    }
}