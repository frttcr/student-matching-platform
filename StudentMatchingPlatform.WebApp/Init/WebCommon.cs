﻿using StudentMatchingPlatform.Common;
using StudentMatchingPlatform.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// It was declared on Global.asax.cs on WebApp layer.
namespace StudentMatchingPlatform.WebApp.Init
{
    public class WebCommon : ICommon
    {
        public string GetEmail()
        {
            if(HttpContext.Current.Session["login"] != null)
            {
                Students student = HttpContext.Current.Session["login"] as Students;
                return student.Email;
            }

            return "system";
        }
    }
}