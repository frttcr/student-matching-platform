﻿using StudentMatchingPlatform.BusinessLayer;
using StudentMatchingPlatform.Entities;
using StudentMatchingPlatform.Entities.Messages;
using StudentMatchingPlatform.Entities.ViewModels;
using StudentMatchingPlatform.WebApp.Filters;
using StudentMatchingPlatform.WebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentMatchingPlatform.WebApp.Controllers
{
    [ExceptionFilter]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                StudentManager studentManager = new StudentManager();

                BusinessLayerResult<Students> result = studentManager.RegisterStudent(model);

                if (result.Errors.Count > 0)
                {
                    result.Errors.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(model);
                }
                OkViewModel notifyObj = new OkViewModel()
                {
                    Title = "Successful Register",
                    Header = "Register is successful! Redirecting...",
                    RedirectingUrl = "/Home/Login",
                };

                notifyObj.Items.Add("Please activate your account from your mail address.");

                return View("Ok", notifyObj);
            }

            return View(model);
        }

        public ActionResult UserActivate(Guid id)
        {
            StudentManager studentManager = new StudentManager();
            BusinessLayerResult<Students> result = studentManager.ActivateStudent(id);

            if(result.Errors.Count > 0)
            {
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Title = "Invalid Account",
                    Header = "Redirecting",
                    Items = result.Errors
                };
            
                return View("Error", errorNotifyObj);
            }

            OkViewModel okNotifyObj = new OkViewModel()
            {
                Title = "Acitved Account",
                Header = "Your account activated.",
                RedirectingUrl = "/Home/Login"
            };

            okNotifyObj.Items.Add("Your account is activated. You can start matching now!");

            return View("Ok", okNotifyObj);
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                StudentManager studentManager = new StudentManager();

                BusinessLayerResult<Students> result = studentManager.ForgotPassword(model);

                if (result.Errors.Count > 0)
                {
                    result.Errors.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(model);
                }
                OkViewModel notifyObj = new OkViewModel()
                {
                    Title = "Successful Operation",
                    Header = "Redirecting...",
                    RedirectingUrl = "/Home/Login",
                };

                notifyObj.Items.Add("You can change your password from your mail address.");

                return View("Ok", notifyObj);
            }

            return View(model);
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(Guid id, ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                StudentManager studentManager = new StudentManager();

                BusinessLayerResult<Students> result = studentManager.ResetPassword(id, model);

                if (result.Errors.Count > 0)
                {
                    result.Errors.ForEach(x => ModelState.AddModelError("", x.Message));
                    return View(model);
                }
                OkViewModel notifyObj = new OkViewModel()
                {
                    Title = "Successful Operation",
                    Header = "Redirecting...",
                    RedirectingUrl = "/Home/Login",
                };

                notifyObj.Items.Add("Your password was changed. You can login now your new password.");

                return View("Ok", notifyObj);
            }

            return View(model);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if(ModelState.IsValid)
            {
                StudentManager studentManager = new StudentManager();
                BusinessLayerResult<Students> result = studentManager.LoginStudent(model);

                if (result.Errors.Count > 0)
                {
                    if (result.Errors.Find(x => x.Code == ErrorMessageCodes.UserIsNotActive) != null)
                    {
                        ViewBag.SetLink = "http://Home//Errors";
                    }

                    result.Errors.ForEach(x => ModelState.AddModelError("", x.Message));

                    return View(model);
                }

                Session["login"] = result.Result;
                //22.04.2019
                return RedirectToAction("InterestArea");
            }
            return View(model);
        }

        [AuthStudent]
        public ActionResult InterestArea()
        {   
            return View();
        }

        [AuthStudent]
        [HttpPost]
        public ActionResult InterestArea(InterestViewModel model)
        {
            Students currentStudent = Session["login"] as Students;

            StudentManager studentManager = new StudentManager();
            BusinessLayerResult<Students> result = studentManager.UpdateInterestArea(currentStudent._id, model);

            if (result.Errors.Count > 0)
            {
                if (result.Errors.Find(x => x.Code == ErrorMessageCodes.OutOfMatchNumber) != null)
                {
                    ErrorViewModel errorNotifyObjects = new ErrorViewModel()
                    {
                        Items = result.Errors,
                        Title = "You can match only one student.",
                        RedirectingUrl = "/Home/Index"
                    };

                    return View("Error", errorNotifyObjects);
                }

                if (result.Errors.Find(x => x.Code == ErrorMessageCodes.NullInfo) != null)
                {
                    ErrorViewModel errorNotifyObjects = new ErrorViewModel()
                    {
                        Items = result.Errors,
                        Title = "Interest area could not add.",
                        RedirectingUrl = "/Home/EditAccount"
                    };

                    return View("Error", errorNotifyObjects); 
                }
            }

            // If there is no problem, algoritm will start when user click the start match button
            studentManager.Algorithm();

            // Session updated because student updated.
            Session["login"] = result.Result;

            return RedirectToAction("InterestArea");
        }

        public ActionResult Logout()
        {
            Session.Clear();

            return RedirectToAction("Index");
        }

        [AuthStudent]
        public ActionResult Account()
        {
            Students currentStudent = Session["login"] as Students;

            StudentManager studentManager = new StudentManager();
          
            BusinessLayerResult<Students> result = studentManager.GetStudentById(currentStudent._id);

            if(result.Errors.Count > 0 )
            {
                // User should go to error page
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Title = "Error",
                    Header = "Redirecting",
                    Items = result.Errors
                };

                return View("Error", errorNotifyObj);
            }

            return View(result.Result);
        }

        //[OutputCache(Duration = 3000, VaryByParam = "none")]
        [AuthStudent]
        public ActionResult EditAccount()
        {
            Students currentStudent = Session["login"] as Students;

            StudentManager studentManager = new StudentManager();
            BusinessLayerResult<Students> result = studentManager.GetStudentById(currentStudent._id);

            UniversityManager universityManager = new UniversityManager();
            BusinessLayerResult<List<Universities>> universityResult = universityManager.GetUniversities();
            
            if (result.Errors.Count > 0 || universityResult.Errors.Count > 0)
            {
                // User should go to error page
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Title = "Error",
                    Header = "Redirecting",
                    Items = result.Errors
                };

                return View("Error", errorNotifyObj);
            }

            EditProfileViewModel editProfileVM = new EditProfileViewModel();

            editProfileVM.Student = result.Result;
            editProfileVM.Universities = universityResult.Result;

            return View(editProfileVM);
        }

        [AuthStudent]
        [HttpPost]
        public ActionResult EditAccount(EditProfileViewModel model)
        {
            StudentManager studentManager = new StudentManager();
            BusinessLayerResult<Students> result = studentManager.UpdateProfile(model);

            if (result.Errors.Count > 0)
            {
                ErrorViewModel errorNotifyObjects = new ErrorViewModel()
                {
                    Items = result.Errors,
                    Title = "Profile could not update.",
                    RedirectingUrl = "/Home/EditAccount"
                };

                return View("Error", errorNotifyObjects);
            }

            // Session updated because student updated.
            Session["login"] = result.Result;

            return RedirectToAction("Account");
        }

        [AuthStudent]
        public ActionResult RemoveAccount()
        {
            Students currentStudent = Session["login"] as Students;

            StudentManager studentManager = new StudentManager();
            BusinessLayerResult<Students> result = studentManager.RemoveUserByID(currentStudent._id);

            if(result.Errors.Count > 0)
            {
                ErrorViewModel errorNotifyObjects = new ErrorViewModel()
                {
                    Items = result.Errors,
                    Title = "Profile could not remove.",
                    RedirectingUrl = "/Home/Account"
                };

                return View("Error", errorNotifyObjects);
            }

            Session.Clear();

            return RedirectToAction("Index");
        }

        [AuthStudent]
        public ActionResult MatchingInfo()
        {
            Students currentStudent = Session["login"] as Students;

            StudentManager studentManager = new StudentManager();
            BusinessLayerResult<Students> result = studentManager.GetStudentMatchingFriend(currentStudent._id);
           
            if (result.Errors.Count > 0)
            {
                // User should go to error page
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Title = "Error",
                    Header = "Redirecting for start matching..",
                    Items = result.Errors,
                    RedirectingUrl = "/Home/InterestArea"
                };

                return View("Error", errorNotifyObj);
            }

            return View(result.Result);

        }

        public ActionResult PreviousMatchInfo()
        {
            Students currentStudent = Session["login"] as Students;

            StudentManager studentManager = new StudentManager();
            List<Students> result = studentManager.GetPreviousStudentMatchingFriend(currentStudent._id);

            ViewData["students"] = result;
            return View();
        }

        [AuthStudent]
        public ActionResult RemoveMatch()
        {
            Students currentStudent = Session["login"] as Students;

            StudentManager studentManager = new StudentManager();
            BusinessLayerResult<Students> result = studentManager.RemoveUserMatchByID(currentStudent._id);

            if (result.Errors.Count > 0)
            {
                ErrorViewModel errorNotifyObjects = new ErrorViewModel()
                {
                    Items = result.Errors,
                    Title = "Matching could not remove.",
                    RedirectingUrl = "/Home/Index"
                };

                return View("Error", errorNotifyObjects);
            }

            // Session updated because student's matching updated.
            Session["login"] = result.Result;

            return RedirectToAction("Index");
        }

        [AuthStudent]
        public ActionResult Messages()
        {
            Students currentStudent = Session["login"] as Students;

            StudentManager studentManager = new StudentManager();
            BusinessLayerResult<Students> result = studentManager.GetStudentMatchingFriend(currentStudent._id);

            if (result.Errors.Count > 0)
            {
                // User should go to error page
                ErrorViewModel errorNotifyObj = new ErrorViewModel()
                {
                    Title = "Error",
                    Header = "Redirecting for start matching..",
                    Items = result.Errors,
                    RedirectingUrl = "/Home/InterestArea"
                };

                return View("Error", errorNotifyObj);
            }

            return View();


        }
        
        public ActionResult ErrorPage()
        {
            return View();
        }
    }
}